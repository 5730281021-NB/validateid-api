const express = require('express')

const app = express();

const bodyParser = require('body-parser');

const isInteger = require("is-integer");

const validate = require('./library/validateid')

var validator = require('validator');

app.listen(8123,function(){
    console.log('app listening on port 8123!');
})

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended : true
}))

app.post('/validateid',async function(req,res){
    if (!req.is('application/json')) {
		st = JSON.parse({
			'RESP_CDE': '402',
			"RESP_MSG": 'Invalid Parameter "JSON Format"'
		});

		res.status(200).send(st);
		return next(
			new errors.InvalidContentError("Expects 'application/json"),
		);
    };
    
    //console.log(req.body.CUST_ID);
    var START = req.body.SELRANGEDT.START;
    var LIMIT = req.body.SELRANGEDT.LIMIT;
    var CUST_ID = req.body.CUST_ID;
    var CONTACT_EMAIL = req.body.CONTACT_EMAIL;
    var CONTACT_MOBILE = req.body.CONTACT_MOBILE;
    //var PARTNER_NBR = req.body.PARTNER_NBR;
    
    if (START < 0 || !isInteger(START)) {
        /*
                st = JSON.parse({
                    'RESP_CDE': '402',
                    'RESP_MSG': 'Invalid Parameter "START"'
                });
        */
                res.status(200).send({
                    'RESP_CDE': '402',
                    'RESP_MSG': 'Invalid Parameter "START"'
                });
                //resfunc("START",res)
            }
        
    if (LIMIT < 0 || !isInteger(LIMIT)) {
        /*
                st = JSON.parse({
                    'RESP_CDE': '402',
                    'RESP_MSG': 'Invalid Parameter "LIMIT"'
                });
        */
                res.status(200).send({
                    'RESP_CDE': '402',
                    'RESP_MSG': 'Invalid Parameter "LIMIT"'
                });
                //resfunc("LIMIT",res)
            }

    if((!(typeof CUST_ID== 'undefined')&&(typeof CONTACT_EMAIL == 'undefined')&&(typeof CONTACT_MOBILE == 'undefined'))) {
        if (!isInteger(CUST_ID) && CUST_ID.length == 13) {
            /*st = JSON.parse({
                'RESP_CDE': '402',
                'RESP_MSG': 'Invalid Parameter "CUST_ID"'
            });*/
            res.status(200).send({'RESP_CDE': '402',
            'RESP_MSG': 'Invalid Parameter "CUST_ID"'})
            //resfunc("CUST_ID",res)
        }
        else {
            Cards =  await validate.validateIDC(CUST_ID);
        };
    }
    else if((!(typeof CONTACT_EMAIL == 'undefined'))&&(typeof CUST_ID == 'undefined')&&(typeof CONTACT_MOBILE == 'undefined')){
        if(!validator.isEmail(CONTACT_EMAIL)){
            res.status(200).send({'RESP_CDE': '402',
            'RESP_MSG': 'Invalid Parameter "CONTACT_EMAIL"'})
        }
        else{
            Cards = await validate.validateIDE(CONTACT_EMAIL);
        }
    }
    else if((!(typeof CONTACT_MOBILE =='undefined'))&&(typeof CONTACT_EMAIL == 'undefined')&&(typeof CUST_ID == 'undefined')){
		var z = Number(CONTACT_MOBILE);
        if(!isInteger(z) || CONTACT_MOBILE.length != 10){
            res.status(200).send({'RESP_CDE': '402',
            'RESP_MSG': 'Invalid Parameter "CONTACT_MOBILE"'})
        }
        else{
            Cards = await validate.validateIDM(z);
        }
    }
    
    
    var seqno =1;

    await clearstart(START,LIMIT,Cards,Cards.length,seqno,res);
})

async function clearstart(START, LIMIT, rows, length, seqno, res) {
	if (START == 0) startzero(LIMIT, rows, length, seqno, res);
	while (START != 0) {
		if (rows.length == 1) {
			res.status(200).send({
				"RESP_CDE": "301",
				"RESP_MSG": "No record in Mcard"
			});
		} else {
			await rows.splice(0, 1);
		}
		START--;
		seqno++;
		if (START == 0) {
			if (rows.length < START) {
				res.status(200).send({
					"RESP_CDE": "301",
					"RESP_MSG": "No record in Mcard"
				});
			} else {
				startzero(LIMIT, rows, length, seqno, res)
			}
		}
	}
}


async function startzero(LIMIT, rows, count, seqno, res) {
	if (LIMIT == 0) {
		returnValidateID(rows.length, rows, count, seqno, res)
	} else {
		if (LIMIT > rows.length) {
			returnValidateID(rows.length, rows, count, seqno, res)
		} else {
			returnValidateID(LIMIT, rows, count, seqno, res)
		}

	}
}

async function returnValidateID(limit, rows, count, seqno, res) {
	var dt = new Date().toISOString();

	var dv = dt.substring(0, 10);

	var date_v = dv.substring(0, 4) + dv.substring(5, 7) + dv.substring(8, 10);

	//logger.debug('Date_V:', date_v);

	var tv = dt.substring(11, 19);
	var hp = tv.substring(0, 2);
	var mp = tv.substring(3, 5);
	var sp = tv.substring(6, 8);

	//console.log('Time-7:', hp);

	hp = parseInt(hp) + 7;

	//console.log('Time+7:', hp);

	if (hp > 23) {

		hp = hp - 24;

	}

	if (hp < 9) {

		hp = '0' + hp.toString();
	} else {

		hp = hp.toString();
	}

	var time_v = hp + mp + sp;
	//logger.debug('Time_V:', time_v);

	var dtf = date_v + time_v;

	var cards = []
	for (var i = 0; i < limit; i++) {
		await cards.push({
		//	"for_test":rows[i].MBDAT,
			"MCARD_NUM": rows[i].MBCODE,
			"CARD_TYPE": rows[i].MBMEMC,
			"CARD_EXPIRY_DATE": rows[i].MBEXP
		});

	}
	if(cards.length==0){
		res.status(200).send({
			"RESP_CDE": "301",
			"RESP_MSG": "No record in Mcard"
		});
	}
	res.status(200).send({
		"RESP_SYSCDE": "200",
		"RESP_DATETIME": dtf,
		"RESP_CDE": "101",
		"RESP_MSG": "success",
		"CARD": cards,
		"RECORDCTRL": {
			"SEQNO": seqno,
			"CARD_COUNT": count
		}
	});

}