const config = require('./config');

module.exports.validateIDC = (async function (CUST_ID) {

    const pool = await new require('node-jt400').pool(config);
    st = await ("Select MBCODE,MBMEMC,MBEXP FROM MBRFLIB/MVM01P WHERE MBID ='"+CUST_ID+"'");
    let q3 = await pool.query(st);
    return q3;

})

module.exports.validateIDE = (async function (Email) {

    const pool = await new require('node-jt400').pool(config);
    st = await ("Select MBCODE,MBMEMC,MBEXP FROM MBRFLIB/MVM01P WHERE MBEMAIL ='"+Email+"'");
    let q3 = await pool.query(st);
    return q3;

})

module.exports.validateIDM = (async function (Mobile) {

    const pool = await new require('node-jt400').pool(config);
    st = await ("Select MBCODE,MBMEMC,MBEXP FROM MBRFLIB/MVM01P WHERE MBPTEL ='"+Mobile+"'");
    let q3 = await pool.query(st);
    return q3;

})
/*module.exports.validateIDP = (async function (NBR) {

    const pool = await new require('node-jt400').pool(config);
    st = await ("Select MBCODE,MBMEMC,MBEXP FROM MBRFLIB/MVM01P WHERE MBPTEL ="+NBR);
    let q3 = await pool.query(st);
    return q3;

})*/